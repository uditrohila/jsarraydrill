function filter(elements,cb){
    if((!Array.isArray(elements)) || cb === undefined){
        return [];
    }
    let arr = [];

    for(let index = 0; index < elements.length; index++){
        let answer = cb(elements[index],index,elements);
        if(answer === true)
        {
            arr.push(elements[index]);
        }
    }

    // console.log(arr);
    return arr;
}



// filter(items,cb);
module.exports = filter;