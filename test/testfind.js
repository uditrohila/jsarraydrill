const find = require("../find");
const items = [1, 2, 3, 4, 5, 5];

function cb(element){
    if(element % 2 === 0)
    return true;
}

const result = find(items,cb);
console.log(result);