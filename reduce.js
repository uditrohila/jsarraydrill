function reduce(elements, cb, startingValue) {
    if((!Array.isArray(elements)) || cb === undefined)
    {
        return [];
    }
    let index;
    if(typeof(startingValue) === 'undefined')
    {
        startingValue = elements[0];
        index = 1;
    }
    else
    {
        index = 0;
    }
    for(;index < elements.length; index++)
        {
            startingValue = cb(startingValue, elements[index],index,elements);
        }

    return startingValue;
}



// let ans = reduce(items,sum,100);
// console.log(ans);

module.exports = reduce;