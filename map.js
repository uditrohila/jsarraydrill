let arr = [];
function map(elements, cb) {
    if((!Array.isArray(elements)) || cb === undefined){
        return [];
    }
    for(let index = 0; index < elements.length; index++) {
        let transformElement = cb(elements[index],index,elements);
        arr.push(transformElement);
    }

    return arr;
    // console.log(arr);
}



// map(items,cb);
module.exports = map;