function flatten(array) {
    if(!(Array.isArray(array))) return [];
    let flatArray = [];
    for(let i =0; i < array.length; i++){
        if(Array.isArray(array[i])) {
            // flatArray.push(...flatten(array[i]));
            flatArray=  flatArray.concat(flatten(array[i]));
        }
        else{
               if(array[i] !== undefined){
                flatArray.push(array[i]);
               }
        }
    }

    return flatArray;
}

// const NewArray = result(nestedArray);
// console.log(NewArray);

module.exports = flatten;