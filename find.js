
function find(elements, cb) {
    if((!Array.isArray(elements)) || cb === undefined)
    {
        return [];
    }
    for(let index = 0; index < elements.length;index++){
        let answer = cb(elements[index],index,elements);
        if(answer === true)
        {
            // return elements[index];
            return index;
            // console.log(elements[index]);
        }
    }

    return undefined;
}



// find(items,cb);
module.exports = find;
