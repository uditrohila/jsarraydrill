
function each(elements, cb) {
    if(!elements || !cb) return;
    for(let index = 0; index < elements.length; index++) {
        cb(elements[index], index,elements);
    }
}


// each(items,cb);

module.exports = each;