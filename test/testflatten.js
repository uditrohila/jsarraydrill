const flatten = require("../flatten.js");
const nestedArray = [1, [2], [[3]], [[[4]]]];
// const nestedArray = [[ 1, 2, [ 3 ], [ [ 4 ] ] ]];
// const nestedArray = [[ 1, 2, undefined, 4, 5 ] ];
// const nestedArray = [[1,2,3], 6 , [7] , [undefined,undefined,undefined]];
// const nestedArray = [ 1, 2, 3, [ 4 ] ];
const ans = flatten(nestedArray);
console.log(ans);